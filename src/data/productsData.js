const productsData = [
    {
        id: "wdc001",
        name: "Graham Balls",
        description: "Graham Powder na may Marshmallow sa gitna tapos binilog na nilagay sa Cupcake Container.",
        price: 5,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Oreo-Pop",
        description: "Oreo na binalot sa IceCream tapos may handle.",
        price: 10,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Pastillas",
        description: "Gatas na binuo at hinulma para maging parang hotdog na pinaliguan ng asukal.",
        price: 55000,
        onOffer: true
    }
]

export default productsData;