import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Masarap na, malinis pa!</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        What we offer is the sweets that you will not just love but will be wanting for more because of its usual ingredients but made with much more. 
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Order Now, Pay Later</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        We have a shop nearby somewhere so you could purchase should you want to satisfy your cravings. We also offer Cash on Delivery for you to have a taste of our specialties in the comfort of your home. 
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Be Part of our Team</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        We offer trainings in-house and would like you to be part of the Eating Team or the Sweetening Team as we provide sweetness throughout the country and could be the rest of the world.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}